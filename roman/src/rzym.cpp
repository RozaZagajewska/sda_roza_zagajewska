#include <iostream>
void romanToArab(char* roman)
{
    int result = 0;
    for(int i = 0; i <= 50; i++)
    {
        if(roman[i] == 'I' && roman[i+1] == 'V')
        {
        	result+=4;
            i++;
        }
        else if(roman[i] == 'I' && roman[i+1] == 'X')
        {
        	result+=9;
            i++;
        }
        else if(roman[i] == 'X' && roman[i+1] == 'L')
        {
        	result+=40;
            i++;
        }
        else if(roman[i] == 'X' && roman[i+1] == 'C')
        {
        	result+=90;
            i++;
        }
        else if(roman[i] == 'C' && roman[i+1] == 'D')
        {
        	result+=400;
            i++;
        }
        else if(roman[i] == 'C' && roman[i+1] == 'M')
        {
        	result+=900;
            i++;
        }
        else if(roman[i] == 'I')
        {
        	result++;
        }
        else if(roman[i] == 'V')
        {
        	result+=5;
        }
        else if(roman[i] == 'X')
        {
        	result+=10;
        }
        else if(roman[i] == 'L')
        {
        	result+=50;
        }
        else if(roman[i] == 'C')
        {
        	result+=100;
        }
        else if(roman[i] == 'D')
        {
        	result+=500;
        }
        else if(roman[i] == 'M')
        {
        	result+=1000;
        }
        else if(!(roman[i]))
        {
            break;
        }
    }
    std::cout << roman << " is " << result << std::endl;
}

int main(int argc, const char * argv[]) {

    char roman[50];
    std::cout << "PROVIDE WITH ROMAN NUMBER(CAPITAL LETTERS): ";
    std::cin >> roman;
    romanToArab(roman);

    return 0;
}
