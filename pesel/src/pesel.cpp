#include <iostream>
#include<string>
using namespace std;
const int weight[] { 1, 3, 7, 9, 1, 3, 7, 9, 1, 3 };
int stringToInt(char ch) {
	return ch - '0';
}

int main() {

	std::cout << "Provide PESEL" << std::endl;
	std::string pesel;
	int sum=0;
	int controlSum=0;
	std::cin >> pesel;
	for (int i = 0; i < 11; i++) {
		cout << pesel[i];
	}
	cout << " checking corectness of PESEL..."<<endl;
	for (int i = 0; i < 10; i++)
	{	int newPesel= stringToInt(pesel[i]);
		std::cout<<newPesel<<endl;
		sum = sum + newPesel * weight[i];

	}
	cout<<sum<<endl;
	controlSum = 10 - (sum % 10);
	cout << "Control sum is:" << controlSum << endl;
	int numberFromPesel=stringToInt(pesel[10]);
	if (controlSum == numberFromPesel) {
		cout << "PESEL correct" << endl;
	} else {
		cout << "PESEL incorrect." << endl;
	}

	return 0;
}
