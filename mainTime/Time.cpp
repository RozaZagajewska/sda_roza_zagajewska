/*
 * Time.cpp
 *
 *  Created on: 18.04.2017
 *      Author: R�a
 */

#include "Time.h"
#include <iostream>
#define maxMin 60
#define minMin 1
#define maxSec 60
#define minSec 1
#define maxHour 24
using namespace std;

Time::Time() {
	mSec = 0;
	mMin = 0;
	mHour = 0;

}
;
Time::Time(int sec, int min, int hour) {
	mSec = sec;
	mMin = min;
	mHour = hour;
	calculate();

}
;
void Time::print() {
	cout << mHour << ":" << mMin << ":" << mSec << endl;
}
void Time::calculate() {

	if (mSec > minSec && mSec <= maxSec) {
		cout << "provided sec: " << mSec << endl;
	} else {
		cout << "Wrong value: ";
	}

	if (mMin > minMin && mMin <= 60) {
		cout << "provided min: " << endl;
	} else {
		cout << "Wrong value: ";
	}

	if (mHour <= 24) {
		cout << "provided hour: " << mHour << endl;
	} else {
		cout << "Wrong value: ";
	}

}
void Time::moveSec(int sec) {
	mSec += sec;
	while (mSec > maxSec) {
		mSec = mSec - maxSec;
		mMin++;
		if (mMin > maxMin) {
			mMin = 1;
			mHour++;
		}
	}

}
void Time::moveMin(int min) {
	mMin += min;
	mHour = mHour + mMin / 60;
	mMin = mMin % 60;
}

void Time::moveHour(int hour) {
	mHour += hour;
}

Time Time::subtractTime(Time time1, Time time2) {
	int sec1, min1, hour1;
	int sec2, min2, hour2;
	sec1 = time1.getSec();
	min1 = time1.getMin();
	hour1 = time1.getHour();
	sec2 = time2.getSec();
	min2 = time2.getMin();
	hour2 = time2.getHour();

	sec1 -= sec2;
	while (sec1 < minSec) {
		min1--;

		if (min1 < minMin) {
			min1 = 59;
			hour1--;
		}
		sec1 += maxSec;
	}

	min1 = min1 - (min2 % 60);
	if (min1 < minMin) {
		min1 += maxMin;
	}
	hour1 = hour1 - (hour1 / 24);
	hour1--;

	hour1 -= hour2;

	cout << "Time after subtracting time2: " << endl;
	return Time(sec1, min1, hour1);

}
;

void Time::subtractSec(int sec) {
	mSec = mSec - sec;
	while (mSec < minSec) {
		mMin--;
		if (mMin < minMin) {
			mMin = 59;
			mHour--;
		}
		mSec += maxSec;
	}
	cout << "Time after subtracting seconds: " << endl;
}
void Time::subtractMin(int min) {
	mMin = mMin - (min % 60);
	if (mMin < minMin) {
		mMin += maxMin;
		mHour--;
	}
	mHour = mHour - (mMin / 60);

	cout << "Time after subtracting seconds: " << endl;
}
void Time::subtractHour(int hour) {
	mHour -= hour;
	cout << "Time after subtracting seconds: " << endl;
}
int Time::getHour() const {
	return mHour;
}

void Time::setHour(int hour) {
	mHour = hour;
}

int Time::getMin() const {
	return mMin;
}

void Time::setMin(int min) {
	mMin = min;
}

int Time::getSec() const {
	return mSec;
}

void Time::setSec(int sec) {
	mSec = sec;
}
void Time::getTime() {
	cout << "Provide sec: " << flush;
	cin >> mSec;
	checkSec();
	cout << "Provide min: " << flush;
	cin >> mMin;
	checkMin();
	cout << "provide hour: " << flush;
	cin >> mHour;
	checkHour();

}
void Time::checkSec() {
	while (mSec< minSec || mSec>=maxSec)
	{
		std::cout << "wrong value" <<std:: endl;
		std::cout << "provide value again" << endl;
		cin>>mSec;
	}
}
void Time::checkMin() {
	while (mMin< minMin || mMin>=60)
	{
		cout << "wrong value" << endl;
		cout << "provide value again" << endl;
		cin>>mMin;
	}
}
void Time::checkHour() {
	while (mHour >= 24)
	{
		cout << "wrong value" << endl;
		cout << "provide value again" << endl;
		cin>>mHour;
	}

}
void Time::setTime() {

}

Time::~Time() {
	// TODO Auto-generated destructor stub
}

