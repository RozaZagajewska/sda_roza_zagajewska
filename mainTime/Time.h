/*
 * Time.h
 *
 *  Created on: 18.04.2017
 *      Author: R�a
 */

#ifndef TIME_H_
#define TIME_H_

class Time {
private:
	void checkSec();
	void checkMin();
	void checkHour();
protected:
	int mSec;
	int mMin;
	int mHour;
public:
	Time();
	Time(int sec, int min, int hour);
	void print();
	void calculate();
	void moveSec(int sec);
	void moveMin(int min);
	void moveHour(int hour);
	Time subtractTime(Time time1, Time time2);
	void subtractSec(int sec);
	void subtractMin(int min);
	void subtractHour(int hour);
	~Time();
	int getHour() const;
	void setHour(int hour);
	int getMin() const;
	void setMin(int min);
	int getSec() const;
	void setSec(int sec);
	void getTime();
	void setTime();
};

#endif /* TIME_H_ */
