/*
 * Jednostki.cpp
 *
 *  Created on: 09.04.2017
 *      Author: R�a
 */
#include "Date.h"

#include<iostream>
#include<string>
#define maxMonth 12
#define minMonth 1
#define minDay 1
using namespace std;

const string monthNames[]={"", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November","December"};


Date::Date() {
	mDay = 0;
	mMonth = 0;
	mYear = 0;

};
Date::Date(int day, int month, int year) {
	mDay = day;
	mMonth = month;
	mYear = year;
	calculate();
};

void Date::calculate() {
	//std::cout << mDay << ".";
	//std::cout << mMonth << ".";
	//std::cout << mYear;

	if(mDay> minDay || mDay<=maxDay())
	{
		cout<<"provided day: "<<mDay<<endl;
	}
	else
	{
		cout<<"Wrong value";
	}

	if(mMonth> maxMonth || mMonth<=12)
	{
		cout<<"provided month: "<<monthNames[mMonth]<<endl;
	}
	else
	{
		cout<<"Wrong value";
	}

	if(mYear>=1900)
	{
		cout<<"provided year: "<<mYear<<endl;
	}
	else
	{
		cout<<"Wrong value";
	}

}
void Date::getDate()
{
	cout<<"Provide day"<<flush;
	cin>>mDay;
	cout<<"Provide month"<<flush;
	cin>>mMonth;
	cout<<"provide year"<<flush;
	cin>>mYear;
	calculate();
	bool ifTrue= leapYear();
	if (ifTrue== true)
	{
		cout<<"leap year"<<endl;

	}
	else
	{
		cout<<"Non-leap year year"<<endl;
	}
}
int Date::maxDay()
{
	int month[]={0,31,28,30,31,30,31,31,31,30,31,30,31};
	int maxDay=month[mMonth];
	if(leapYear()&&mMonth==2)
	{
		maxDay+=1;
	}
		return maxDay;

}
void Date::print()
{
	cout<<mDay<<" "<<monthNames[mMonth]<<" "<<mYear<<" "<<endl;
}

bool Date::leapYear()
{	bool result=false;
	if (mYear%400==0 || (mYear %4==0 && mYear%100 !=0))
	{
		result=true;
	}
	return result;
}
;
Date::~Date(){};
int Date::getDay() const {
	return mDay;
}

void Date::setDay(int day) {
	cout<<"provide day"<<flush;
	cin>>mDay;
}

int Date::getMonth() const {
	return mMonth;
}

void Date::setMonth(int month) {
	cout<<"provide month"<<flush;
	cin>>mMonth;
}

int Date::getYear() const {
	return mYear;
}

void Date::setYear(int year) {
	cout<<"provide year"<<flush;
	cin>>mYear;
}

void Date::moveDay(int day)
{
	mDay += day;
	 while (mDay> maxDay())
	 {
		 mDay= mDay-maxDay();
		 mMonth ++;
		 if(mMonth>maxMonth)
		 {
			 mMonth=1;
			 mYear++;
		 }
	 }
}
void Date::moveMonth(int month)
{
	mMonth =+ month;
	mYear = mYear+ mMonth/12;
	mMonth= mMonth%12;


}
void Date::moveYear(int year)
{
	mYear=+year;
}
Date Date::subtractDate(Date date1, Date date2)
{
	int day1, month1, year1;
	int day2, month2, year2;
	day1=date1.getDay();
	month1=date1.getMonth();
	year1=date1.getYear();
	day2=date2.getDay();
	month2=date2.getMonth();
	year2=date2.getYear();

	day1-=day2;
	while (day1< minDay)
		 {
			month1 --;

			 if(month1<minMonth)
			 {
				 month1=12;
				 year1--;
			 }
			 day1+=maxDay();
		 }


	month1=month1-(month2%12);
		if(month1<minMonth)
		{
			month1+=maxMonth;
		}
		year1 = year1- (month1/12);
		year1--;

		year1-=year2;

	return Date(day1, month1, year1);
}
void Date::subtractDay(int day)
{
	mDay-=day;
	while (mDay< minDay)
		 {
			mMonth --;

			 if(mMonth<minMonth)
			 {
				 mMonth=12;
				 mYear--;
			 }
			 mDay+=maxDay();
		 }

}
void Date::subtractMonth(int month)
{
	//mMonth-=month;

	mMonth=mMonth-(month%12);
	if(mMonth<minMonth)
	{
		mMonth+=maxMonth;
		mYear--;
	}
	mYear = mYear- (mMonth/12);


	cout<<mMonth<<endl;
}
void Date::subtractYear(int year)
{
	mYear-=year;
}

