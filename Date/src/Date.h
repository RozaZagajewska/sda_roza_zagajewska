/*
 * Jednostki.h
 *
 *  Created on: 09.04.2017
 *      Author: R�a
 */

#ifndef DATE_H_
#define DATE_H_

class Date {
private:
	bool leapYear();
	int maxDay();
protected:
	int mDay;
	int mMonth;
	int mYear;
public:
	Date();
	Date(int day, int month, int year);
	void calculate();
	void getDate();
	void print();
	void moveDay(int day);
	void moveMonth(int month);
	void moveYear(int year);
	Date subtractDate(Date date1, Date date2);
	void subtractDay(int day);
	void subtractMonth(int month);
	void subtractYear(int year);

	~Date();
	int getDay() const;
	void setDay(int day);
	int getMonth() const;
	void setMonth(int month);
	int getYear() const;
	void setYear(int year);
};

#endif /* DATE_H_ */
